# ZSH Minimal Theme        
PROMPT='$(git_prompt_info)'
PROMPT+='$fg_bold[white]%}%n:%{$fg_bold[cyan]%}%c%{$reset_color%} $ '

ZSH_THEME_GIT_PROMPT_PREFIX="\uE0A0%{$fg_bold[green]%}"
ZSH_THEME_GIT_PROMPT_DIRTY=""
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%} "