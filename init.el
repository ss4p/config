;; [ Emacs Config ]

;;(require 'package)
;;(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))

;;(dashboard-setup-startup-hook)
;;(ac-config-default)
;;(doom-modeline-mode 1)

;; Change Comment foreground
(set-face-foreground 'font-lock-comment-face "#7c756c")

;; Load Editor Theme
(add-to-list 'custom-theme-load-path "~/.emacs.d/atom-one-dark-theme")
(load-theme 'atom-one-dark t)

;; Editor View
(global-display-line-numbers-mode)
(setq display-line-numbers-type 'relative)
(global-hl-line-mode)
(menu-bar-mode -1)
(tool-bar-mode -1)
(setq c-default-style "linux")
(setq c-basic-offset 4)
(c-set-offset 'comment-intro 0)

;; Emacs Auto Backup
(setq backup-directory-alist `(("." . "~/.saves")))
(package-initialize)

;; Indentation
(setq-default indent-tabs-mode nil)
(setq-default tab-width 4)
(setq indent-line-function 'insert-tab)

;; My Emacs key binding
(global-set-key (kbd "C-t") 'tab-new)
(global-set-key (kbd "M-s") 'tab-switcher)
(global-set-key (kbd "C-x C-q") 'tab-close)
